@extends('layout')
@section('title', 'Task')

@section('content')
  <h1>TO DO LIST</h1>
      <div id="button">
      <a href="/task/create">Add a Task</a>
    </div>
      <table border=1>
        <tr>
          <th>Taskname</th>
          <th>Taskdescription</th>
          <th>Status</th>
          <th>Edit</th>
          <th>Delete</th>
          </tr>

          @foreach($data as $row)
          <tr>
           <td>{{$row->taskname}}</td>
           <td>{{$row->taskdescription}}</td>
           <td>
           @php
           if($row->taskstatus==0) echo "TODO";
           elseif($row->taskstatus==1) echo "DONE";
           @endphp
            </td>
           <td><a href="/task/{{$row->id}}/edit">EDIT</a></td>
           <form method='post' action="/task/{{$row->id}}">
            @csrf
            @method('delete')
           <td><input type="submit" value="DELETE" name="delete"</td>
         </tr>
       </form>

         @endforeach






        </table>

    @endsection

@extends('layout')
@section('title', 'Add Task')

<head>
  <style>
  #form
  {
    margin-left:300px;
    margin-top:200px;
  }
  </style>
</head>


@section('sidebar')
    @parent


@endsection
@section('content')
<div id="form">
<form method='post' action="/task/{{$a->id}}">
  @csrf
  @method('put')
  Taskname:<input value="{{$a->taskname}}" type="text" name="task"/><br/>
  Taskdescription:<input value="{{$a->taskdescription}}" type="text" name="description"/><br/>
  Taskstatus:

  <input type="radio" name="status" id="status" @if($a->taskstatus==0) checked @endif value="0"/>TODO
  <input type="radio" name="status" id="status"  @if($a->taskstatus==1) checked @endif value="1"/>DONE
<br/>
  <input type="submit" name="submit" value="Add"/>
</form>
</div>

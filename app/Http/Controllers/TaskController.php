<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



       $data=Task::all();

      return view('task',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name=$request->task;
        $description=$request->description;
       $status=$request->status;
       $task=new Task();  //This is model//$task represents an object
       $task->taskname=$name;
       $task->taskdescription=$description;
       $task->taskstatus=$status;
       $task->save();
       return redirect('/task');
       echo $name;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $a=Task::find($id);

        return view('edit',compact('a'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $name=$request->task;
      $description=$request->description;
     $status=$request->status;
     $array=['taskname'=>$name,'taskdescription'=>$description,'taskstatus'=>$status];
     $b=Task::find($id);//find row corresponding to the id
     $b->update($array);
     return redirect('/task');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $c=Task::find($id);
        $c->delete();
         return redirect('/task');
    }
}

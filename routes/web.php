<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
  //echo "hello";

  return view('login');
});
Route::post('/login','IndexController@request');

Route::resource('/index','IndexController');

Route::resource('/task','TaskController');
